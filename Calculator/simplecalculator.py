# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 12:43:34 2020

@author: Killian Massé
"""


class SimpleCalculator:
    """
    Classe des opérations
    """
    def somme(self, nombre1, nombre2):
        """
        fonction somme
        """
        return nombre1 + nombre2

    def soustraction(self, nombre1, nombre2):
        """
        fonction soustraction
        """
        return nombre1 - nombre2

    def division(self, nombre1, nombre2):
        """
        fonction division
        """
        return nombre1 / nombre2

    def multiplication(self, nombre1, nombre2):
        """
        fonction multiplication
        """
        return nombre1 * nombre2

if __name__ == "__main__":
    """
    Simple test in case of main call
    """
    RES = SimpleCalculator()  # creation d'un objet de ma classe
    print(RES.somme(130, 23))
    print(RES.soustraction(130, 23))
    print(RES.division(130, 23))
    print(RES.multiplication(130, 23))
