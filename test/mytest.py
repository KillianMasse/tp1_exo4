# -*- coding: utf-8 -*-
from Calculator.simplecalculator import SimpleCalculator as SimpleCalculator
"""
Created on Mon Mar 23 12:43:34 2020

@author: Killian Massé
"""

if __name__ == "__main__":
    """
    Simple test in case of main call
    """

    RES = SimpleCalculator()  # creation d'un objet de ma classe
    print(RES.somme(130, 23))
    print(RES.soustraction(130, 23))
    print(RES.division(130, 23))
    print(RES.multiplication(130, 23))
